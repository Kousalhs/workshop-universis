import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {GradesService} from '../../services/grades.service';
import { GradeScaleService } from '@universis/common';
import {TranslateModule} from '@ngx-translate/core';
import {NgPipesModule} from 'ngx-pipes';
import {SharedModule} from '@universis/common';
import {ModalModule, TooltipModule} from 'ngx-bootstrap';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {MostModule} from '@themost/angular';
import { ConfigurationService } from '@universis/common';
import {LoadingService} from '@universis/common';
import {GradesAllComponent} from './grades-all.component';
import {TestingConfigurationService} from '../../../test';
import {APP_BASE_HREF} from '@angular/common';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {ChartsModule} from 'ng2-charts';
import {FilterByPipe} from 'ngx-pipes';

describe('GradesAllComponent', () => {
    let component: GradesAllComponent;
    let fixture: ComponentFixture<GradesAllComponent>;
    const loadingSvc = jasmine.createSpyObj('LoadingService', ['showLoading' , 'hideLoading' ]);

    const gradeSvc = jasmine.createSpyObj('GradesService', ['getAllGrades', 'getCourseTeachers', 'getGradeInfo',
                                          'getDefaultGradeScale', 'getThesisInfo', 'getLastExamPeriod', 'getRecentGrades',
                                          'getGradesSimpleAverage', 'getGradesWeightedAverage']);
    const gradeScaleSvc = jasmine.createSpyObj('GradeScaleService', ['getGradeScales', 'getGradeScale']);


    // Mocks response to make 'this.data.filter' work in component's ngOnInit
    gradeSvc.getGradeInfo.and.returnValue(Promise.resolve(JSON.parse('[{"course":{"courseStructureType": 4}},{"course":{"courseStructureType": 1}},{"course":{"courseStructureType": 2}}]')));
    gradeSvc.getDefaultGradeScale.and.returnValue(Promise.resolve(0));

    beforeEach(async(() => {
        return TestBed.configureTestingModule({
            declarations: [GradesAllComponent],
            providers: [
                {
                    provide: GradesService,
                    useValue: gradeSvc
                },
                {
                    provide: GradeScaleService,
                    useValue: gradeScaleSvc
                },
                {
                    provide: ConfigurationService,
                    useClass: TestingConfigurationService
                },
                {
                  provide: LoadingService,
                  useValue: loadingSvc
                },
                {
                    provide: APP_BASE_HREF,
                    useValue: '/'
                },
              FilterByPipe
            ],
            imports: [
              ChartsModule,
                HttpClientTestingModule,
                TranslateModule.forRoot(),
                MostModule.forRoot({
                    base: '/',
                    options: {
                        useMediaTypeExtensions: false
                    }
                }),
                SharedModule,
                NgPipesModule,
                ModalModule.forRoot(),
                TooltipModule.forRoot()
            ],
          schemas: [
            CUSTOM_ELEMENTS_SCHEMA
          ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(GradesAllComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
