import { Component, OnInit } from '@angular/core';
import {GradesService} from '../../../grades/services/grades.service';
import {ConfigurationService} from '@universis/common';

@Component({
  selector: 'app-student-recent-grades',
  templateUrl: './student-recent-grades.component.html',
  styleUrls: ['./student-recent-grades.component.scss']
})
export class StudentRecentGradesComponent implements OnInit {

  public latestCourses: any = [];
  public isLoading = true;   // Only if data is loaded
  public defaultLanguage = null;
  public currentLanguage = null;

  constructor(private gradesService: GradesService, private _configurationService: ConfigurationService) {
    this.currentLanguage = this._configurationService.currentLocale;
    this.defaultLanguage = this._configurationService.settings.i18n.defaultLocale;
  }

  ngOnInit() {
    this.gradesService.getRecentGrades().then((res) => {
      this.latestCourses = res;
      if (Array.isArray(this.latestCourses)) {
        this.latestCourses.sort((a, b) => a.course.name.localeCompare(b.course.name));
      }
      this.isLoading = false; // Data is loaded
    });

  }

}
